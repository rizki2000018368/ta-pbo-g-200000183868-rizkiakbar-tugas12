package com.uad;

public class Transaksi extends Pemesan implements barang {

    private int kode_barang, kdTrans, banyak;
    private String nama_barang;
    private long harga;
    private double diskon;


    public Transaksi(int No_id, String Nama, String Alamat, String No_tlp) {
        super(No_id, Nama, Alamat, No_tlp);
    }

    public void setTransaksi(int kdTrans, String date, int banyak) {
        this.kdTrans = kdTrans;
        this.banyak=banyak;
        }

    public double getTotal(int jml, long harga, double diskon) {
        return jml * harga * diskon / 100;
    }
        public void setbarang (int kode_barang, String nama_barang, long harga, double diskon){
            this.kode_barang = kode_barang;
            this.nama_barang = nama_barang;
            this.harga = harga;
            this.diskon = diskon;
        }

    public void showStruk() {

        System.out.println("Kode Transaksi :"+kdTrans);
        System.out.println("================================");
        System.out.println("Kode Pelanggan  :"+getNo_id());
        System.out.println("Nama Pelanggan  :"+getNama());
        System.out.println("Alamat          :"+getAlamat());
        System.out.println("=================================");
        System.out.println("Kode Barang    :"+kode_barang);
        System.out.println("Nama Barang    :"+nama_barang);
        System.out.println("Harga Barang   :"+harga);
        System.out.println("Diskon : "+diskon);
        System.out.println("\nBanyak Pembelian :"+banyak);
        System.out.println("================================");
        System.out.println("Total          :"+getTotal(banyak, harga, diskon));
    }


}



