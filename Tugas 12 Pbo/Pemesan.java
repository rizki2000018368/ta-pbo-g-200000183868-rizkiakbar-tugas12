package com.uad;

public class Pemesan {
    private int Noid;
    private String Nama,Alamat,No_telp;

    public Pemesan(int No_id, String Nama, String Alamat, String No_tlp) {
        this.Noid = No_id;
        this.Nama = Nama;
        this.Alamat = Alamat;
        this.No_telp = No_tlp;
    }


    public int getNo_id() {
        return Noid;
    }

    public String getNama() {
        return Nama;
    }

    public String getAlamat() {
        return Alamat;
    }

    public String getNo_telp() {
        return No_telp;
    }


}
